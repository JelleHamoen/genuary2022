let particles = [];
let c1, c2, c3, c3tria;
let colors = [];
let xOffset = 0;
let yOffset = 0;
let angleFactor = 0;

function setup() {
    createCanvas(720, 720);
    xOffset = int(random(5000));
    yOffset = int(random(5000));
    angleFactor = random(4);
    console.log(`Angle factor: ${angleFactor}`)
    // perspective(PI / 3.0, width / height, 0.1, 500);
    // frameRate(2);
    c1 = color("#39ff14");
    c2 = color("#ff14af");
    c3 = color('#1F51FF');
    c3tria = color(255, 93, 31);
    colors = [c3, c3tria]
    for (var i = 0; i < 50; i++) {
        particles.push(new particle(int(random(width)), int(random(height)), random(colors)))
    }
    strokeWeight(3);
}

function draw() {
    blendMode(BLEND);

    background(233,221,209);
    stroke(c3tria);
    noFill();
    blendMode(HARD_LIGHT);
    for (var i = 0; i < particles.length; i++) {
        if (particles[i].dead === false) {
            particles[i].move();
        }
        particles[i].drawToCanvas();
    }
}

class particle {
    constructor(x, y, c) {
        this.x = x;
        this.y = y;
        this.c = c;
        this.dx = x;
        this.dy = y;
        this.dead = false;
        this.points = [];

    }

    move() {
        if (this.dx < width && this.dx > 0 && this.dy < height && this.dy > 0) {
            let angle = noise(this.dx + xOffset , this.dy + yOffset) * TWO_PI * angleFactor;
            let v = p5.Vector.fromAngle(angle,10);
            this.dx = this.dx + v.x;
            this.dy = this.dy + v.y;
            let p = createVector(this.dx, this.dy);
            this.points.push(p);
        } else {
            // console.log(`Dead at x:${this.dx},y:${this.dy}`);
            this.dead = true;
        }

    }

    drawToCanvas() {
        push();
        stroke(this.c);
        beginShape();
        for (let i = 0; i < this.points.length; i++) {
            curveVertex(this.points[i].x, this.points[i].y)
        }
        endShape();
        pop();
    }
}

