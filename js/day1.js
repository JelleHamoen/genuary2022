function setup() {
    createCanvas(720, 720);
    // frameRate(2);
    noFill();
    noStroke();
    c1 = color("#39ff14");
    c2 = color("#ff14af");
    c3 = color('#1F51FF');
    colors = [ c2, c3];
    blendMode(OVERLAY);
}

function draw() {
    blendMode(BLEND);
    background(230);
    translate(10, 10);
    blendMode(BURN);
    for (var i = 0; i < 100; i++) {
        for (var j = 0; j < 100; j++) {
            fill(random(colors));
            dx = random(-2, 10);
            dy = random(-2, 10);
            r = random(5, 15);
            circle(7 * i + dx, 7 * j + dy, r);
        }
    }
}