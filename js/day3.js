let stars = [];
let c1, c2, c3, c3tria;
let colors = [];

function setup() {
    createCanvas(700, 700, WEBGL);
    // perspective(PI / 3.0, width / height, 0.1, 500);
    // frameRate(2);
    c1 = color("#39ff14");
    c2 = color("#ff14af");
    c3 = color('#1F51FF');
    c3tria = color(255, 93, 31);
    colors = [c3tria];

    for (let i = 0; i < 50; i++) {
        stars.push(new Star(int(random(-250, 250)), int(random(-250, 250)), 0, int(random(50)), random(colors)));
    }
    frameRate(30);
}

function draw() {
    lightFalloff(1, 0, 0)
    ambientLight(100, 50, 50);
    background(10, 0, 0);
    rotateY(millis() / 2000);
    rotateX(millis() / 4000);
    rotateZ(millis() / 6000);
    pointLight(250, 100, 100, 0, 0, 0);
    noStroke();
    ambientMaterial(c3tria);
    strokeWeight(0.3);

    torus(350);
    stroke(60, 60, 60);

    for (let i = 0; i < stars.length; i++) {
        stars[i].move();
    }

}

class Star {
    constructor(x, y, z, r, c) {
        this.y = y;
        this.x = x;
        this.z = z;
        this.c = c;
        this.r = r;
    }

    move() {
        this.r = this.r - 1;
        // this.x = this.x - 5;
        // this.y = this.y - 5;
        fill(255, 93, 31);
        translate(this.x, this.y, this.r * 10)
        sphere(this.r);
        translate(-this.x, -this.y, -this.r * 10)

        if (this.r <= 0) {
            this.r = int(random(50));
            this.x = int(random(-250, 250));
            this.y = int(random(-250, 250));
            return;
        }
        return true;
    }

}