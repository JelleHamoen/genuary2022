let c1, c2, c3, c4, c5, bg;
let colors = [];
let font;
let justStarted = true;
let framesSinceStarted = 0;

function preload() {
    font = loadFont("../font/Courier_New.otf");
}

function setup() {
    createCanvas(720, 720);
    frameRate(10);

    c1 = color("#39ff14");
    c2 = color("#ff14af");
    c3 = color("#1F51FF");
    c4 = color(255);
    c5 = color(10, 0, 0);
    bg = color("#363b40");
    colors = [c1, c2, c3, c4, c5]
    fill(c1);
    textFont(font);
    noStroke();
    background(random(colors));
    textSize(64);
}

function draw() {
    fill(bg);
    if (justStarted) {
        if (frameCount % 10 === 0) {
            text('Be there', random(width) - 200, random(height) + 10);
            framesSinceStarted = framesSinceStarted + 20;
        }
        if (framesSinceStarted > 100) {
            justStarted = false;
        }
    } else {
        text('Be there', random(width) - 200, random(height) + 10);
    }

}

function keyTyped() {
    if (keyCode === 32) {
        background(random(colors));
        justStarted = true;
        framesSinceStarted = 0;
    }
}

