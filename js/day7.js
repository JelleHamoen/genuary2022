let c1, c2, c3, c4, c5, bg;
let colors = [];
let font;
let dEye = 140; //how wide the eyes open
let tEye = 15;
let eyeColor;

function preload() {
    font = loadFont("../font/Courier_New.otf");
}

function setup() {
    createCanvas(1040, 480);
    frameRate(30);

    c1 = color("#39ff14");
    c2 = color("#ff14af");
    c3 = color("#1F51FF");
    c4 = color(255);
    c5 = color(10, 0, 0);
    bg = color("#363b40");
    colors = [c1, c2, c3, c4, c5]
    fill(c1);
    textFont(font);
    noStroke();
    background(0);
    textSize(64);
    alert("EPILEPSY WARNING, please don't watch this if you cannot stand rapidly flashing colors")
}

function draw() {
    eyeColor = random(colors);
    background(0);
    translate(0, 60);
    fill(255);
    rect(20, 20, 320, 320);
    drawLeftEye();
    fill(255);
    rect(700, 20, 320, 320);
    drawRightEye();
    fill(255);
    rect(360, 20, 320, 320);
}

function drawLeftEye() {
    push();
    translate(20, 20);
    beginShape();
    fill(0)
    vertex(20, 20);
    vertex(300 - sin(frameCount / tEye) * dEye, 20 + sin(frameCount / tEye) * dEye);
    vertex(300, 300);
    vertex(20 + sin(frameCount / tEye) * dEye, 300 - sin(frameCount / tEye) * dEye);
    endShape(CLOSE);
    pop();
    push();
    translate(52, 52);
    beginShape();
    fill(eyeColor)
    vertex(20, 20);
    scale(0.8, 0.8);

    vertex(300 - sin(frameCount / tEye) * dEye, 20 + sin(frameCount / tEye) * dEye);
    vertex(300, 300);
    vertex(20 + sin(frameCount / tEye) * dEye, 300 - sin(frameCount / tEye) * dEye);
    endShape(CLOSE);
    pop();
}

function drawRightEye() {
    push();
    translate(700, 20);
    beginShape();
    fill(0)
    vertex(300 - sin(frameCount / tEye) * dEye, 300 - sin(frameCount / tEye) * dEye);
    vertex(20, 300);
    vertex(20 + sin(frameCount / tEye) * dEye, 20 + sin(frameCount / tEye) * dEye);
    vertex(300, 20);
    endShape(CLOSE);
    pop();
    fill(eyeColor);
    push();
    translate(732, 52);
    scale(0.8, 0.8);
    beginShape();
    vertex(300 - sin(frameCount / tEye) * dEye, 300 - sin(frameCount / tEye) * dEye);
    vertex(20, 300);
    vertex(20 + sin(frameCount / tEye) * dEye, 20 + sin(frameCount / tEye) * dEye);
    vertex(300, 20);
    endShape(CLOSE);
    pop();

}

